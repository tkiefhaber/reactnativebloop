import React, { Component } from 'react';
import { AppRegistry, Image, View, Text, StyleSheet, ListView } from 'react-native';

export default class MockTomatoes extends Component {

  fetchData() {
    const API_KEY = '465daa0714bdd659c1291cad3745d247';
    const API_URL = 'https://api.themoviedb.org/3/discover/movie/';
    const PAGE_SIZE = 25;
    const PARAMS = `?api_key=${API_KEY}&primary_release_date.gte=2017-01-17&primary_release_date.lte=2017-08-01`;
    const REQUEST_URL = API_URL + PARAMS;
    console.log(REQUEST_URL);
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState(
          {dataSource: this.state.dataSource.cloneWithRows(responseData.results), loaded: true}
        )
      }).done()
  };

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    }
  }

  componentDidMount() {this.fetchData()}

  renderMovie(movie) {
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      },
      thumbnail: {
        width: 53,
        height: 81,
      },
      rightContainer: {
        flex: 1,
      },
      title: {
        fontSize: 20,
        marginBottom: 8,
        alignItems: 'flex-start',
        paddingLeft: 10,
      },
      year: {
        alignItems: 'flex-start',
        paddingLeft: 10,
      },
    });

    return (
      <View style={styles.container} key={movie.id}>
        <Image style={styles.thumbnail} source={{uri: `https://image.tmdb.org/t/p/w500${movie.poster_path}`}} />
        <View style={styles.rightContainer}>

          <Text style={styles.title}>{movie.title}</Text>
          <Text style={styles.year}>{movie.release_date}</Text>
        </View>
      </View>
    );
  }

  renderLoadingView() {
    return(
      <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
        <Text>Loading Movies...</Text>
      </View>
    );
  }

  render() {
    if (this.state.loaded) {
      return (
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderMovie}
          style={{flex:1, paddingTop: 20, backgroundColor: 'papayawhip'}}
        />
      )
    } else {
      return this.renderLoadingView();
    }
  }
}


AppRegistry.registerComponent('ReactNativeBloop', () => MockTomatoes)

